﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneticAlgorithmArchi
{
    public class Individual : AbstractIndividual<int>
    {
        public Individual(int count)
            : base(count) 
        {

        }

        public override void GenerateInvividual()
        {
            for (int i = 0; i < Count; i++)
            {
                int gene = rand.Next(0, 10);
                _genes[i] = gene;
            }
        }

        public override double GetFitness(int[] solution = null)
        {
            fitness = 0;
            // Loop through our individuals genes and compare them to our cadidates
            for (int i = 0; i < Count && i < solution.Length; i++)
            {
                if (this[i].Equals(solution[i]))
                {
                    fitness++;
                }
            }
            return fitness;
        }

        public override double GetValue()
        {
            throw new NotImplementedException();
        }

        public override void Mutate(double mutationRate)
        {
            for (int i = 0; i < Count; i++)
            {
                if (rand.NextDouble() <= mutationRate)
                {
                    // Create random gene
                    int gene = rand.Next(0, 10);
                    this[i] = gene;
                }
            }
        }
    }
}
