﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneticAlgorithmArchi.Travel
{
    public class TourManager
    {
        private static List<City> destinationCities = new List<City>();

        public static void AddCity(City city)
        {
            destinationCities.Add(city);
        }

        // Get a city
        public static City GetCity(int index)
        {
            return destinationCities[index];
        }

        // Get the number of destination cities
        public static int numberOfCities()
        {
            return destinationCities.Count;
        }

    }
}
