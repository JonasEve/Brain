﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneticAlgorithmArchi.Travel
{
    public class Tour : AbstractIndividual<City>
    {
        private double distance = 0;

        public Tour(int count) : base(count)
        {
        }

        public override void GenerateInvividual()
        {
            for (int i = 0; i < Count; i++)
            {
                City gene = TourManager.GetCity(i);
                _genes[i] = gene;
            }
        }

        public override double GetFitness(City[] solutions = null)
        {
            if (fitness == 0)
            {
                fitness = 1 / GetValue();
            }
            return fitness;
        }

        public override void Mutate(double mutationRate)
        {
            for (int tourPos1 = 0; tourPos1 < _genes.Length; tourPos1++)
            {
                // Apply mutation rate
                if (rand.NextDouble() < mutationRate)
                {
                    // Get a second random position in the tour
                    int tourPos2 = (int)(_genes.Length * rand.NextDouble());

                    // Get the cities at target position in tour
                    City city1 = _genes[tourPos1];
                    City city2 = _genes[tourPos2];

                    // Swap them around
                    _genes[tourPos2] = city1;
                    _genes[tourPos1] = city2;
                }
            }
        }

        public override double GetValue()
        {
            if (distance == 0)
            {
                double tourDistance = 0;
                // Loop through our tour's cities
                for (int cityIndex = 0; cityIndex < _genes.Length; cityIndex++)
                {
                    // Get city we're travelling from
                    City fromCity = _genes[cityIndex];
                    // City we're travelling to
                    City destinationCity;
                    // Check we're not on our tour's last city, if we are set our 
                    // tour's final destination city to our starting city
                    if (cityIndex + 1 < _genes.Length)
                    {
                        destinationCity = _genes[cityIndex + 1];
                    }
                    else
                    {
                        destinationCity = _genes[0];
                    }
                    // Get the distance between the two cities
                    tourDistance += fromCity.DistanceTo(destinationCity);
                }
                distance = tourDistance;
            }
            return distance;
        }

        public override void CrossOver(AbstractIndividual<City> father, AbstractIndividual<City> mother, double uniformRate)
        {
            int rate = (int)(1 / uniformRate);
            int offset = rand.Next(0, father.Count / rate);
            int count = 0;

            if (father.GetFitness() > mother.GetFitness())
            {
                for (int i = offset; i <= father.Count / rate; i++)
                {
                    _genes[i - offset] = father[i];
                    count++;
                }

                for (int i = 0; i < mother.Count; i++)
                {
                    if(!_genes.Contains(mother[i]))
                        _genes[count++] = mother[i];
                }
            }

            else
            {
                for (int i = offset; i <= mother.Count / rate; i++)
                {
                    _genes[i - offset] = mother[i];
                    count++;
                }     

                for (int i = 0; i < father.Count; i++)
                {
                    if (!_genes.Contains(father[i]))
                    {
                        _genes[count++] = father[i];
                    }
                }
            }
        }
    }
}
