﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneticAlgorithmArchi.Travel
{
    public class City
    {
        private Random rand = new Random();
        private int v1;
        private int v2;

        public int X { get; }
        public int Y { get; }

        public City()
        {
            X = (int)(rand.NextDouble() * 200);
            Y = (int)(rand.NextDouble() * 200);
        }

        public City(int x, int y)
        {
            X = x;
            Y = y;
        }

        public double DistanceTo(City city)
        {
            int xDistance = Math.Abs(X - city.X);
            int yDistance = Math.Abs(Y - city.Y);
            double distance = Math.Sqrt((xDistance * xDistance) + (yDistance * yDistance));

            return distance;
        }

        public override String ToString()
        {
            return X + "," + Y;
        }
    }
}
