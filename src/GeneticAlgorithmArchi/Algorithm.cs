﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneticAlgorithmArchi
{
    public static class Algorithm<Population, AbstractIndividual, T>
        where AbstractIndividual : AbstractIndividual<T>
        where Population : Population<AbstractIndividual, T>
    {
        private const double uniformRate = 0.5;
        private const double mutationRate = 0.015;
        private const int tournamentSize = 5;
        private const bool elitism = true;
        private static Random rand = new Random();

        public static Population<AbstractIndividual, T> EvolvePopulation(Population<AbstractIndividual, T> pop, T[] solution = null)
        {
            Population<AbstractIndividual, T> newPopulation = new Population<AbstractIndividual, T>(pop.Count, false);

            // Keep our best individual
            if (elitism)
            {
                newPopulation[0] = pop.GetFittest(solution);
            }

            // Crossover population
            int elitismOffset;
            if (elitism)
            {
                elitismOffset = 1;
            }
            else
            {
                elitismOffset = 0;
            }
            // Loop over the population size and create new individuals with
            // crossover
            for (int i = elitismOffset; i < pop.Count; i++)
            {
                AbstractIndividual indiv1 = tournamentSelection(pop, solution);
                AbstractIndividual indiv2 = tournamentSelection(pop, solution);

                AbstractIndividual newSol1 = (AbstractIndividual)Activator.CreateInstance(typeof(AbstractIndividual), indiv1.Count);
                newSol1.CrossOver(indiv1, indiv2, uniformRate);

                AbstractIndividual newSol2 = (AbstractIndividual)Activator.CreateInstance(typeof(AbstractIndividual), indiv1.Count);
                newSol2.CrossOver(indiv1, indiv2, uniformRate);

                var familly = new Population<AbstractIndividual, T>(2, false);

                familly[0] = newSol1;
                familly[1] = newSol2;
                //familly[2] = indiv1;
                //familly[3] = indiv2;

                newPopulation[i] = familly.GetFittest(solution);
            }

            // Mutate population
            newPopulation.Mutate(elitismOffset, mutationRate);

            return newPopulation;
        }


        // Select individuals for crossover
        private static AbstractIndividual tournamentSelection(Population<AbstractIndividual, T> pop, T[] solution = null)
        {
            // Create a tournament population
            Population<AbstractIndividual, T> tournament = new Population<AbstractIndividual, T>(tournamentSize, false);
            // For each place in the tournament get a random individual
            for (int i = 0; i < tournamentSize; i++)
            {
                int randomId = (int)(rand.NextDouble() * pop.Count);
                tournament[i] = pop[randomId];
            }
            // Get the fittest
            AbstractIndividual fittest = tournament.GetFittest(solution);
            return fittest;
        }
    }
}
