﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneticAlgorithmArchi
{
    public abstract class AbstractIndividual<T>
    {

        protected Random rand = new Random();
        protected T[] _genes;
        // Cache
        protected double fitness = 0;

        public AbstractIndividual(int count)
        {
            _genes = new T[count];
        }

        public virtual T[] Genes
        {
            get
            {
                return (T[])_genes.Clone();
            }

        }

        public virtual T this[int index]
        {
            get
            {
                return _genes[index];
            }

            set
            {
                _genes[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return _genes.Length;
            }
        }

        public abstract void GenerateInvividual();

        public abstract void Mutate(double mutationRate);

        public abstract double GetFitness(T[] solutions = null);

        public abstract double GetValue();

        public virtual void CrossOver(AbstractIndividual<T> father, AbstractIndividual<T> mother, double uniformRate)
        {
            // Loop through genes
            for (int i = 0; i < father.Count; i++)
            {
                // Crossover
                if (rand.NextDouble() <= uniformRate)
                {
                    _genes[i] = father[i];
                }
                else
                {
                    _genes[i] = mother[i];
                }
            }
        }
    }
}
