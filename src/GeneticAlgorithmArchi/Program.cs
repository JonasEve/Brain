﻿using GeneticAlgorithmArchi;
using GeneticAlgorithmArchi.Travel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmArchi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            /*int[] solution = { 1, 2, 1, 6, 8, 9, 3, 7, 4, 6, 3, 5};

            // Create an initial population
            Population<Individual, int> myPop = new Population<Individual, int>(50, true, solution.Length);

            // Evolve our population until we reach an optimum solution
            int generationCount = 0;
            do
            {
                generationCount++;
                Console.WriteLine("Generation: " + generationCount + " Fittest: " + myPop.GetFittest(solution).GetFitness(solution));
                myPop = Algorithm<Population<Individual, int>, Individual, int>.EvolvePopulation(myPop, solution);

            } while (myPop.GetFittest(solution).GetFitness(solution) < solution.Length);

            Console.WriteLine("Solution found!");
            Console.WriteLine("Generation: " + generationCount);
            Console.WriteLine("Genes:");
            Console.WriteLine(ConvertStringArrayToString(myPop.GetFittest(solution).Genes));

            Console.ReadKey();*/


            // Create and add our cities
            City city = new City(60, 200);
            TourManager.AddCity(city);
            City city2 = new City(180, 200);
            TourManager.AddCity(city2);
            City city3 = new City(80, 180);
            TourManager.AddCity(city3);
            City city4 = new City(140, 180);
            TourManager.AddCity(city4);
            City city5 = new City(20, 160);
            TourManager.AddCity(city5);
            /*City city6 = new City(100, 160);
            TourManager.AddCity(city6);
            City city7 = new City(200, 160);
            TourManager.AddCity(city7);
            City city8 = new City(140, 140);
            TourManager.AddCity(city8);
            City city9 = new City(40, 120);
            TourManager.AddCity(city9);
            City city10 = new City(100, 120);
            TourManager.AddCity(city10);
            City city11 = new City(180, 100);
            TourManager.AddCity(city11);
            City city12 = new City(60, 80);
            TourManager.AddCity(city12);
            City city13 = new City(120, 80);
            TourManager.AddCity(city13);
            City city14 = new City(180, 60);
            TourManager.AddCity(city14);
            City city15 = new City(20, 40);
            TourManager.AddCity(city15);
            City city16 = new City(100, 40);
            TourManager.AddCity(city16);
            City city17 = new City(200, 40);
            TourManager.AddCity(city17);
            City city18 = new City(20, 20);
            TourManager.AddCity(city18);
            City city19 = new City(60, 20);
            TourManager.AddCity(city19);
            City city20 = new City(160, 20);
            TourManager.AddCity(city20);*/

            // Initialize population
            Population<Tour, City> pop = new Population<Tour, City>(50, true, TourManager.numberOfCities());
            Console.WriteLine("Initial distance: " + pop.GetFittest().GetValue());

            // Evolve population for 100 generations
            pop = Algorithm<Population<Tour, City>, Tour, City>.EvolvePopulation(pop);
            for (int i = 0; i < 10000; i++)
            {
                pop = Algorithm<Population<Tour, City>, Tour, City>.EvolvePopulation(pop);
            }

            // Print final results
            Console.WriteLine("Finished");
            Console.WriteLine("Final distance: " + pop.GetFittest().GetValue());
            Console.WriteLine("Solution:");
            Console.WriteLine(ConvertStringArrayToString(pop.GetFittest().Genes));

            Console.ReadKey();
        }

        public static string ConvertStringArrayToString<T>(T[] array)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder builder = new StringBuilder();
            foreach (T value in array)
            {
                builder.Append(value);
                builder.Append(" -> ");
            }

            return builder.ToString();
        }
    }
}
