﻿using GeneticAlgorithmArchi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GeneticAlgorithmArchi
{
    public class Population<AbstractIndividual, T>
        where AbstractIndividual : AbstractIndividual<T>
    {
        private AbstractIndividual[] individuals;

        public AbstractIndividual this[int index]
        {
            get
            {
                return individuals[index];
            }
            set
            {
                individuals[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return individuals.Length;
            }
        }

        public Population(int populationSize, bool initialise, int genesByInd = 0)
        {
            individuals = new AbstractIndividual[populationSize];
            // Initialise population
            if (initialise)
            {
                // Loop and create individuals
                for (int i = 0; i < populationSize; i++)
                {
                    AbstractIndividual newIndividual = (AbstractIndividual)Activator.CreateInstance(typeof(AbstractIndividual), genesByInd);
                    newIndividual.GenerateInvividual();
                    individuals[i] = newIndividual;
                }
            }
        }

        public AbstractIndividual GetFittest(T[] solution = null)
        {
            AbstractIndividual fittest = individuals[0];
            // Loop through individuals to find fittest
            for (int i = 1; i < individuals.Length; i++)
            {
                AbstractIndividual individual = individuals[i];

                if (fittest.GetFitness(solution) < individuals[i].GetFitness(solution))
                {
                    fittest = individual;
                }
            }
            return fittest;
        }

        public void Mutate(int elitismOffset, double mutationRate)
        {
            for (int i = elitismOffset; i < individuals.Length; i++)
            {
                individuals[i].Mutate(mutationRate);
            }
        }
    }
}
