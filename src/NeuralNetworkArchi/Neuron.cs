﻿using NeuralNetworkArchi.ActivationFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuralNetworkArchi
{
    public class Neuron<ActivationFunction>
        where ActivationFunction : IActivationFunction
    {
        public double[] Inputs { get; set; }
        public double[] Weights { get; set; }
        public double Error { get; set; }

        private double[] weightsDiff;
        private double biasWeight;
        private double biasWeightDiff;
        private Layer<ActivationFunction> layer;

        private ActivationFunction activationFunction;

        private Random r = new Random();

        public Neuron(int nbOfInputs, Layer<ActivationFunction> layer)
        {
            Inputs = new double[nbOfInputs];
            Weights = new double[nbOfInputs];
            weightsDiff = new double[nbOfInputs];
            this.layer = layer;

            activationFunction = Activator.CreateInstance<ActivationFunction>();
        }

        private double midOutput
        {
            get
            {
                double value = 0;

                for (int i = 0; i < Inputs.Length; i++)
                {
                    value += Weights[i] * Inputs[i];
                }

                return value + biasWeight;
            }
        }

        public double Output
        {
            get
            {
                if(activationFunction.NeedOtherOutput())
                    return activationFunction.Output(midOutput, layer.Neurons.Select(n => n.midOutput).ToArray());
                else
                    return activationFunction.Output(midOutput);
            }
        }

        public void RandomizeWeights()
        {
            for (int i = 0; i < Weights.Length; i++)
            {
                Weights[i] = r.NextDouble();
                weightsDiff[i] = 0;
            }

            biasWeight = r.NextDouble();
            biasWeightDiff = 0;
        }

        public void AdjustWeights(double learnRate, double momentum)
        {
            double delta = 0;
            for (int i = 0; i < Weights.Length; i++)
            {
                delta = learnRate * Error * Inputs[i];
                Weights[i] += delta + momentum * weightsDiff[i];
                weightsDiff[i] = delta;
            }

            delta = Error * learnRate;
            biasWeight += delta + momentum * biasWeightDiff;
            biasWeightDiff = delta;
        }
    }
}
