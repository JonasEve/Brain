﻿using NeuralNetworkArchi.ActivationFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuralNetworkArchi
{
    public class Layer<ActivationFunction>
        where ActivationFunction : IActivationFunction
    {
        private Neuron<ActivationFunction>[] neurons;

        public Layer(int nbNeuron, int nbOfInputs)
        {
            neurons = new Neuron<ActivationFunction>[nbNeuron];

            for (int n = 0; n < neurons.Length; n++)
            {
                neurons[n] = new Neuron<ActivationFunction>(nbOfInputs, this);

                // random weights
                neurons[n].RandomizeWeights();
            }
        }

        public int Count
        {
            get
            {
                return neurons.Length;
            }
        }

        public Neuron<ActivationFunction> this[int index]
        {
            get
            {
                return neurons[index];
            }

            set
            {
                neurons[index] = value;
            }
        }

        public Neuron<ActivationFunction>[] Neurons
        {
            get
            {
                return (Neuron<ActivationFunction>[])neurons.Clone();
            }
        }

        public void AdjustWeights(double learnRate, double momentum)
        {
            for (int n = 0; n < neurons.Length; n++)
            {
                neurons[n].AdjustWeights(learnRate, momentum);
            }
        }
    }
}
