﻿using NeuralNetworkArchi.ActivationFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuralNetworkArchi
{
    public class Program
    {
        const int nbOfInputs = 15;
        const int nbHiddenLayer = 1;
        const int nbHiddenNeurons = 10;
        static int[] nbHiddenNeuronsArray = new int[] {3, 3, 7, 5, 3 };
        const int nbOuputNeurons = 3;
        static Action<double> log = (error) => { Console.WriteLine("Error = {0:0.000}", error); };

        const int epoch = 10000;
        const double threshold = 0.5;

        public static void Main(string[] args)
        {

            List<Pattern> patternsTraining = Helpers.LoadPatterns("Training.csv", nbOfInputs, nbOuputNeurons);
            List<Pattern> patternsTest = Helpers.LoadPatterns("Tests.csv", nbOfInputs, nbOuputNeurons);

            var network = new Network<Sigmoid, Sigmoid>(nbOfInputs, nbHiddenLayer, nbHiddenNeuronsArray, nbOuputNeurons);

            Console.WriteLine("Training Begin");
            network.Train(patternsTraining.Select(p => p.Inputs).ToArray(), patternsTraining.Select(p => p.Output).ToArray(), epoch, log);
            Console.WriteLine("Training Finished : Error = {0:0.000}", network.Error);

            Console.WriteLine("Tests Begin");
            network.Do(patternsTest.Select(p => p.Inputs).ToArray(), patternsTest.Select(p => p.Output).ToArray());
            Console.WriteLine("Tests Finished : Error = {0:0.000}", network.Error);

            Console.ReadKey();


            /*double[] data = { 1, 1, 1 };

            double[][] input2 =
            {
                new double[] {  1, 1, 1,
                                1, 0, 1,
                                0, 0 ,1,
                                1, 0 ,1,
                                1, 1 ,1},
                 new double[] { 0, 0, 1,
                                0, 0, 1,
                                0, 0 ,0,
                                0, 0 ,1,
                                0, 0 ,1} ,
                 new double[] { 1, 1, 1,
                                0, 0, 1,
                                1, 1 ,1,
                                1, 0 ,0,
                                1, 0 ,1} ,
                 new double[] { 1, 1, 1,
                                0, 0, 1,
                                1, 1 ,1,
                                0, 0 ,1,
                                1, 1 ,1} ,
                 new double[] { 1, 0, 1,
                                1, 0, 1,
                                1, 1 ,1,
                                0, 0 ,1,
                                0, 0 ,1} ,
                 new double[] { 1, 1, 1,
                                1, 0, 0,
                                1, 1 ,1,
                                0, 0 ,1,
                                1, 1 ,1} ,
                 new double[] { 1, 0, 1,
                                1, 0, 0,
                                1, 1 ,1,
                                1, 0 ,1,
                                1, 1 ,1} ,
                 new double[] { 1, 1, 0,
                                0, 1, 0,
                                1, 0 ,1,
                                0, 1 ,0,
                                0, 1 ,0}
            };

            while (true)
            {
                int one = Helpers.KeyToInt(Console.ReadKey());

                data = input2[one];

                data = network.Do(data, threshold);

                Console.WriteLine(" => " + Helpers.ConvertStringArrayToString(data));
            }*/
        }
    }
}
