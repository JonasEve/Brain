﻿using NeuralNetworkArchi.ActivationFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuralNetworkArchi
{
    public class Network<ActivationFunctionHidden, ActivationFunctionOutput>
        where ActivationFunctionHidden: IActivationFunction
        where ActivationFunctionOutput: IActivationFunction

    {
        private Layer<ActivationFunctionHidden>[] hiddenLayers;
        private Layer<ActivationFunctionOutput> outputLayer;
        public double Error { get; private set; }

        public Network(int nbOfInputs, int nbHiddenLayer, int nbNeuronByLayer, int nbOuputNeurons)
            :this(nbOfInputs, nbHiddenLayer, new int[] { nbNeuronByLayer}, nbOuputNeurons)
        {

        }

        public Network(int nbOfInputs, int nbHiddenLayer, int[] nbNeuronByLayer, int nbOuputNeurons)
        {
            hiddenLayers = new Layer<ActivationFunctionHidden>[nbHiddenLayer];

            hiddenLayers[0] = new Layer<ActivationFunctionHidden>(nbNeuronByLayer[0], nbOfInputs);

            if (nbNeuronByLayer.Length == 1)
            {
                for (int i = 1; i < hiddenLayers.Length; i++)
                {
                    hiddenLayers[i] = new Layer<ActivationFunctionHidden>(nbNeuronByLayer[0], hiddenLayers[i - 1].Count);
                }
            }
            else
            {
                for (int i = 1; i < hiddenLayers.Length; i++)
                {
                    hiddenLayers[i] = new Layer<ActivationFunctionHidden>(nbNeuronByLayer[i], hiddenLayers[i - 1].Count);
                }
            }

            outputLayer = new Layer<ActivationFunctionOutput>(nbOuputNeurons, hiddenLayers.Last().Count);
        }

        public void Train(double[][] inputs, double[][] results, int epochs, Action<double> log = null, double learnRate = 1, double momentum = 0.1)
        {
            for (int j = 0; j < epochs; j++)
            {
                train(inputs, results, learnRate, momentum);
                if(log != null)
                    log.Invoke(Error);
            }
        }

        public void Train(double[][] inputs, double[][] results, double errorGoal, Action<double> log = null, double learnRate = 1, double momentum = 0.1)
        {
            do
            {
                train(inputs, results, learnRate, momentum);
                if (log != null)
                    log.Invoke(Error);
            } while (Error >= errorGoal);
        }

        private void train(double[][] inputs, double[][] results, double learRate, double momentum)
        {
            Error = 0;
            for (int i = 0; i < inputs.Length; i++)
            { // very important, do NOT train for only one example

                forwardPropagation(inputs[i]);

                backwardPropagation(results[i]);

                adjustWeights(learRate, momentum);
            }
        }

        public double[,] Do(double[][] inputs, double[][] wantedResults = null)
        {
            Error = 0;
            double[,] results = new double[inputs.Length,outputLayer.Count];

            for (int i = 0; i < inputs.Length; i++)
            {
                forwardPropagation(inputs[i]);

                for (int j = 0; j < outputLayer.Count; j++)
                {
                    results[i,j] = outputLayer[j].Output;
                    if (wantedResults != null)
                    {
                        Error += Math.Pow((wantedResults[i][j] - results[i,j]), 2);
                    }
                }
            }

            return results;
        }

        private void forwardPropagation(double[] inputs)
        {
            for (int n = 0; n < hiddenLayers[0].Count; n++)
            {
                hiddenLayers[0][n].Inputs = new double[inputs.Length];
                for (int inp = 0; inp < inputs.Length; inp++)
                    hiddenLayers[0][n].Inputs[inp] = inputs[inp];
            }

            for (int l = 1; l <= hiddenLayers.Length; l++)
            {
                if (l == hiddenLayers.Length)
                {
                    Compute(hiddenLayers[l - 1], outputLayer);
                }
                else
                {
                    Compute(hiddenLayers[l - 1], hiddenLayers[l]);
                }
            }
        }

        private void Compute(Layer<ActivationFunctionHidden> hiddenLayer, Layer<ActivationFunctionOutput> outputLayer)
        {
            for (int n = 0; n < outputLayer.Count; n++)
            {
                outputLayer[n].Inputs = new double[hiddenLayer.Count];
                for (int inp = 0; inp < hiddenLayer.Count; inp++)
                    outputLayer[n].Inputs[inp] = hiddenLayer[inp].Output;
            }
        }

        private void Compute(Layer<ActivationFunctionHidden> hiddenLayer, Layer<ActivationFunctionHidden> outputLayer)
        {
            for (int n = 0; n < outputLayer.Count; n++)
            {
                outputLayer[n].Inputs = new double[hiddenLayer.Count];
                for (int inp = 0; inp < hiddenLayer.Count; inp++)
                    outputLayer[n].Inputs[inp] = hiddenLayer[inp].Output;
            }
        }

        private void backwardPropagation(double[]results)
        {
            ActivationFunctionOutput activationFunctionOutput = Activator.CreateInstance<ActivationFunctionOutput>();
            ActivationFunctionHidden activationFunctionHidden = Activator.CreateInstance<ActivationFunctionHidden>();
            // adjusts the weight of the output neuron, based on its error     
            for (int k = 0; k < outputLayer.Count; k++)
            {
                double delta = (results[k] - outputLayer[k].Output);
                outputLayer[k].Error = delta * activationFunctionOutput.Derivative(outputLayer[k].Output);
                //outputLayer[k].adjustWeights(3);
                Error += Math.Pow(delta, 2);
            }

            for (int l = hiddenLayers.Length - 1; l >= 0; l--)
            {
                // then adjusts the hidden neurons' weights, based on their errors
                for (int n = 0; n < hiddenLayers[l].Count; n++)
                {
                    double sum = 0.0; // need sums of output signals times hidden-to-output weights

                    int count = (l == hiddenLayers.Length - 1) ? outputLayer.Count : hiddenLayers[l + 1].Count;
                    for (int k = 0; k < count; k++)
                    {
                        sum += (l == hiddenLayers.Length - 1) ? (outputLayer[k].Error * outputLayer[k].Weights[n]) : (hiddenLayers[l + 1][k].Error * hiddenLayers[l + 1][k].Weights[n]);
                        //sum += outputLayer[k].error * outputLayer[k].weights[n];
                    }

                    var derivative = activationFunctionHidden.Derivative(hiddenLayers[l][n].Output);
                    hiddenLayers[l][n].Error = derivative * sum;
                    //hiddenLayers[l][n].adjustWeights(3);
                }
            }
        }

        private void adjustWeights(double learnRate, double momentum)
        {
            outputLayer.AdjustWeights(learnRate, momentum);

            for (int l = 0; l < hiddenLayers.Length; l++)
            {
               hiddenLayers[l].AdjustWeights(learnRate, momentum);
            }
        }
    }
}
