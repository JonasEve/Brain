﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuralNetworkArchi.ActivationFunction
{
    public interface IActivationFunction
    {
        bool NeedOtherOutput();
        double Output(double x, params double[] y);
        double Derivative(double x);
    }
}
