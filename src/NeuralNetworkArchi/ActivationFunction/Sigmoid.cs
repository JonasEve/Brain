﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuralNetworkArchi.ActivationFunction
{
    public class Sigmoid : IActivationFunction
    {
        public double Output(double x, params double[] y)
        {
            if (x < -45.0) return 0.0;
            else if (x > 45.0) return 1.0;

            return 1.0 / (1.0 + Math.Exp(-x));
        }

        public double Derivative(double x)
        {
            return x * (1 - x);
        }

        public bool NeedOtherOutput()
        {
            return false;
        }
    }
}
