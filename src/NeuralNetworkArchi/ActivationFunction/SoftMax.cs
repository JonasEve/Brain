﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuralNetworkArchi.ActivationFunction
{
    public class SoftMax : IActivationFunction
    {
        public double Derivative(double x)
        {
            return (1 - x) * x;
        }

        public bool NeedOtherOutput()
        {
            return true;
        }

        public double Output(double x, params double[] y)
        {
            double sum = 0;

            for(int i = 0; i < y.Length; i++)
            {
                sum += Math.Exp(y[i]);
            }

            return Math.Exp(x) / sum;
        }
    }
}
