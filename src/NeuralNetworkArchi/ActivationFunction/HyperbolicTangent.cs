﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NeuralNetworkArchi.ActivationFunction
{
    public class HyperbolicTangent : IActivationFunction
    {
        public double Derivative(double x)
        {
            return (1 + x) * (1 - x);
        }

        public bool NeedOtherOutput()
        {
            return false;
        }

        public double Output(double x, params double[] y)
        {
            if (x < -45.0) return -1.0;
            else if (x > 45.0) return 1.0;

            return Math.Tanh(x);
        }
    }
}
