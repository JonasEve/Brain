﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNetworkArchi
{
    public static class Helpers
    {
        public static string ConvertStringArrayToString(double[] array)
        {
            //
            // Concatenate all the elements into a StringBuilder.
            //
            StringBuilder builder = new StringBuilder();
            foreach (double value in array)
            {
                builder.Append(value);
            }

            return builder.ToString();
        }


        public static int KeyToInt(ConsoleKeyInfo UserInput)
        {
            // We check input for a Digit
            if (char.IsDigit(UserInput.KeyChar))
            {
                return int.Parse(UserInput.KeyChar.ToString()); // use Parse if it's a Digit
            }
            else
            {
                return -1;  // Else we assign a default value
            }
        }

        public static List<Pattern> LoadPatterns(string name, int inputSize, int outputsize)
        {
            List<Pattern>  _patterns = new List<Pattern>();
            using (StreamReader file = File.OpenText(name))
            {
                while (!file.EndOfStream)
                {
                    string line = " ";
                    do
                    {
                        line += file.ReadLine();

                    } while (!line.Last().Equals(';'));

                    _patterns.Add(new Pattern(line.Substring(0, line.Length - 1), inputSize, outputsize));
                }
            }

            return _patterns;
        }
    }
}
