﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace NeuralNetworkArchi
{
    public class Pattern
    {
        private double[] _inputs;
        private double[] _output;

        public Pattern(string value, int inputSize, int outputSize)
        {
            string[] line = value.Split(':');

            string[] inputLine = line[0].Split(',');

            string[] outputLine = line[1].Split(',');

            if (inputLine.Length != inputSize)
                throw new Exception("Input does not match network configuration");
            if (outputLine.Length != outputSize)
                throw new Exception("Output does not match network configuration");

            _inputs = new double[inputSize];
            _output = new double[outputSize];
            for (int i = 0; i < inputSize; i++)
            {
                _inputs[i] = double.Parse(inputLine[i], CultureInfo.InvariantCulture);
            }
            for (int i = 0; i < outputSize; i++)
            {
                _output[i] = double.Parse(outputLine[i], CultureInfo.InvariantCulture);
            }
        }

        public double[] Inputs
        {

            get { return _inputs; }
        }

        public double[] Output
        {
            get { return _output; }
        }
    }
}
